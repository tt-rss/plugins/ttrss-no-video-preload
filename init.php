<?php
class Af_Zz_NoPreload extends Plugin {

	function about() {
		return array(null,
			"Don't preload HTML5 videos (for slow connections)",
			"fox");
	}

	function init($host) {
		//
	}

	function get_js() {
		return file_get_contents(__DIR__ . "/init.js");
	}

	function api_version() {
		return 2;
	}

}
