/* global require, PluginHost */

require(['dojo/_base/kernel', 'dojo/ready'], function  (dojo, ready) {
	function unpreload(vid) {
		vid.setAttribute("preload", "none");
	}

	function unpreload_all(row) {
		[...row.querySelectorAll("video")].forEach((vid) => unpreload(vid));
	}

	ready(function () {
		PluginHost.register(PluginHost.HOOK_ARTICLE_RENDERED_CDM, function (row) {
			unpreload_all(row);
			return true;
		});

		PluginHost.register(PluginHost.HOOK_ARTICLE_RENDERED, function (row) {
			unpreload_all(row);
			return true;
		});
	});
});
